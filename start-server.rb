#!/usr/bin/env ruby

require 'webrick'

# https://gist.github.com/jstarrdewar/3805501
class NonCachingFileHandler < WEBrick::HTTPServlet::FileHandler
  def prevent_caching(res)
    res['ETag']          = nil
    res['Last-Modified'] = Time.now + 100**4
    res['Cache-Control'] = 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0'
    res['Pragma']        = 'no-cache'
    res['Expires']       = Time.now - 100**4
  end

  def do_GET(req, res)
    super
    prevent_caching(res)
  end
end

# Like running `ruby -run -e httpd . -p 9090` but without any caching:
server = WEBrick::HTTPServer.new(Port: 9090)
server.mount('/', NonCachingFileHandler, Dir.pwd)
trap('INT') { server.stop }
server.start
