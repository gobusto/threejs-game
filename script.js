import GameEngine from './src/GameEngine.js'

window.onload = function () {
  'use strict'

  const game = new GameEngine('#game-area')

  // Chrome refuses to load or play sounds until the user has interacted with
  // the page in some way, so require the user to explicitly enable it:
  document.getElementById('enable-audio').onclick = (event) => {
    event.preventDefault()
    game.enableAudio()
  }

  // Switch to fullscreen mode, if requested to by the user:
  document.getElementById('enable-fullscreen').onclick = (event) => {
    event.preventDefault()
    game.enableFullscreen()
  }

  game.mainLoop()
}
