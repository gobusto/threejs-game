class HeightGrid {
  // Specify the number of sample points (not tiles!) across and along:
  constructor (x, y) {
    this.xSize = Math.floor(x)
    this.ySize = Math.floor(y)

    this.point = []
    for (var i = 0; i < this.xSize * this.ySize; ++i)
      this.point.push(0)
  }

  // Set the height of a specific point using a pair of INTEGER values:
  setPoint (x, y, value) {
    if (x < 0 || x >= this.xSize || y < 0 || y >= this.ySize)
      return false

    this.point[(Math.floor(y) * this.xSize) + Math.floor(x)] = Number(value)
    return true
  }

  // Used internally; converts "7.34" into 7, 8, and 0.34.
  _clamp (numPoints, value) {
    if (value <= 0)
      return { p1: 0, p2: 0, lerp: 0 }
    else if (value >= numPoints - 1)
      return { p1: numPoints - 1, p2: numPoints - 1, lerp: 0 }

    const f = Math.floor(value)
    return { p1: f, p2: f + 1, lerp: value - f }

  }

  // Get the Z height and X/Y gradients of a point using a pair of REAL values:
  getPoint (x, y) {
    x = this._clamp(this.xSize, x)
    y = this._clamp(this.ySize, y)

    const a = this.point[(y.p1 * this.xSize) + x.p1]
    const b = this.point[(y.p1 * this.xSize) + x.p2]
    const c = this.point[(y.p2 * this.xSize) + x.p1]
    const d = this.point[(y.p2 * this.xSize) + x.p2]

    if (x.lerp + y.lerp < 1) {
      // Lower-left triangle:
      const mx = b - a
      const my = c - a
      const z = a + (mx * x.lerp) + (my * y.lerp)
      return { z: z, mx: mx, my: my }
    } else {
      // Upper-right triangle:
      const mx = c - d
      const my = b - d
      const z = d + (mx * (1 - x.lerp)) + (my * (1 - y.lerp))
      return { z: z, mx: -mx, my: -my }
    }
  }
}

export default HeightGrid
