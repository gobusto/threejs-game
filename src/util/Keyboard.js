class Keyboard {
  constructor () {
    this.updateState = this.updateState.bind(this)

    // This represents the current state of the keyboard. Each key is stored as
    // `{ "ARROWUP": true, "W": false }` so unknown keys are implicitly `null`.
    this.state = {}

    // This determines whether we have exclusive control of keyboard events. It
    // may be useful to prevent arrow keys from scrolling a page, but note that
    // it will also prevent you from pressing F12 to open the browser devtools!
    this.exclusive = false
  }

  // This updates the state in response to a keyup/down event:
  updateState (event) {
    this.state[event.key.toUpperCase()] = (event.type === 'keydown')
    if (this.exclusive) { event.preventDefault() }
    return false
  }

  // Attach listeners to a given DOM element, such as `document.body`:
  attach (element) {
    element.addEventListener('keyup', this.updateState, true)
    element.addEventListener('keydown', this.updateState, true)
  }

  // Clear previously-attached listeners for a given DOM element:
  clear (element) {
    element.removeEventListener('keyup', this.updateState, true)
    element.removeEventListener('keydown', this.updateState, true)
  }

  enableExclusiveMode () { this.exclusive = true }
  disableExclusiveMode () { this.exclusive = false }

  getState () { return this.state }
}

export default Keyboard
