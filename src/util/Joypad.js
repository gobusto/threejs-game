class Joypad {
  constructor () { this.state = null }

  // Chrome maps common pads to the "standard" layout, but Firefox doesn't.
  kind () {
    if (!this.state) return null
    // Debian / Chromium / 360, PS3, and PS4 pads:
    else if (this.state.mapping == 'standard') return 'standard'
    // Debian / Firefox / XBox 360 pad:
    else if (this.state.id.indexOf('360') >= 0) return '360'
    // Debian / Firefox / XBox One pad:
    else if (this.state.id.indexOf('Microsoft X-Box One') >= 0) return 'xb1'
    // Debian / Firefox / PS3 pad:
    else if (this.state.id.indexOf('PLAYSTATION(R)3') >= 0) return 'ps3'
    // Debian / Firefox / PS4 pad:
    else if (this.state.id.indexOf('Sony') >= 0) return 'ps4'
    // Anything else is unrecognised:
    else return 'unknown'
  }

  leftStickX () { return this.state ? this.state.axes[0] : 0 }
  leftStickY () { return this.state ? this.state.axes[1] : 0 }

  rightStickX () {
    if (!this.state) return 0
    return this.state.axes[this.state.mapping === 'standard' ? 2 : 3]
  }

  rightStickY () {
    if (!this.state) return 0
    return this.state.axes[this.state.mapping === 'standard' ? 3 : 4]
  }

  // This is the A button on an Xbox 360 controller.
  face1 () {
    return this.state ? this.state.buttons[0].pressed : false
  }

  // This is the B button on an Xbox 360 controller.
  face2 () {
    return this.state ? this.state.buttons[1].pressed : false
  }

  // This is the X button on an Xbox 360 controller.
  face3 () {
    if (!this.state) return false
    if (this.kind() === 'ps3') return this.state.buttons[3].pressed
    if (this.kind() === 'ps4') return this.state.buttons[3].pressed
    return this.state.buttons[2].pressed
  }

  // This is the Y button on an Xbox 360 controller.
  face4 () {
    if (!this.state) return false
    if (this.kind() === 'ps3') return this.state.buttons[2].pressed
    if (this.kind() === 'ps4') return this.state.buttons[2].pressed
    return this.state.buttons[3].pressed
  }

  // Get the current state of the first attached joypad we find.
  update () {
    this.state = null

    const devices = (navigator.getGamepads && navigator.getGamepads()) || []
    for (var i = 0; i < devices.length; ++i) {
      if (devices[i] && devices[i].connected) {
        this.state = devices[i]
        return true
      }
    }

    return false
  }
}

export default Joypad
