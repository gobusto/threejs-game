class MeshLoader {
  load (meshUrl, skinUrl) {
    return skinUrl ? this.loadMTL(meshUrl, skinUrl) : this.loadOBJ(meshUrl)
  }

  nameAndPathFor (uri) {
    const path = uri.split('/')
    const file = path.pop()
    const base = path.join('/') + '/'
    return { base: base, file: file }
  }

  loadOBJ (meshUrl, materials) {
    return new Promise((resolve, reject) => {
      const uri = this.nameAndPathFor(meshUrl)
      const objLoader = new THREE.OBJLoader()

      if (materials) {
        materials.preload()
        objLoader.setMaterials(materials)
      }

      objLoader.setPath(uri.base).load(uri.file, resolve, null, reject)
    })
  }

  loadMTL (meshUrl, skinUrl) {
    return new Promise((resolve, reject) => {
      const uri = this.nameAndPathFor(skinUrl)
      const mtlLoader = new THREE.MTLLoader()

      mtlLoader.setPath(uri.base).load(
        uri.file,
        materials => {
          this.loadOBJ(meshUrl, materials).then(resolve, reject)
        },
        null,
        error => {
          console.log("loadMTL() - Could not load", skinUrl)
          this.loadOBJ(meshUrl).then(resolve, reject)
        }
      )
    })
  }
}

export default MeshLoader
