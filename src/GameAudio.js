class GameAudio {
  constructor () {
    this.listener = new THREE.AudioListener()
    this.turn = 0

    this.music = new THREE.Audio(this.listener)
    this.sounds = {}
    this.channels = []
  }

  setListenerPosition (x, y, z, turn, _look, _roll) {
    this.listener.position.set(x, y, z)
    this.turn = turn
    this.channels.forEach(sound => this._updateSound(sound))
  }

  loadSound (name, url) {
    if (this.sounds[name]) { return false }

    // A temporary placeholder value to satisfy the check above during loading:
    this.sounds[name] = 'loading'

    this._loadFile(url).then(
      audio => this.sounds[name] = audio,
      error => console.log('Could not load', name, '-', error)
    )

    return true
  }

  playSound (name, settings) {
    const audio = this.sounds[name]
    if (!audio || audio === 'loading') { return null }

    let sound = null
    if (settings && Array.isArray(settings.position)) {
      sound = new THREE.PositionalAudio(this.listener)
      sound.position.set(
        settings.position[0] || 0,
        settings.position[1] || 0,
        settings.position[2] || 0,
      )
      this._updateSound(sound)
    } else {
      sound = new THREE.Audio(this.listener)
    }

    sound.setBuffer(audio)
    if (settings && settings.loop) { sound.setLoop(true) }
    sound.play()

    this.channels = this.channels.filter(channel => channel.isPlaying)
    this.channels.push(sound)

    return sound.uuid
  }

  stopSound (soundID) {
    const sound = this.channels.find(channel => channel.uuid === soundID)
    if (!sound) { return false }
    sound.stop()
    return true
  }

  setSoundPosition (soundID, x, y, z) {
    const sound = this.channels.find(channel => channel.uuid === soundID)
    if (!sound || !sound.panner) { return false } // Not a 3D positional sound.

    sound.position.set(x, y, z)
    this._updateSound(sound)

    return true
  }

  setSoundVolume (soundID, value) {
    const sound = this.channels.find(channel => channel.uuid === soundID)
    if (!sound) { return false }
    sound.setVolume(value)
    return true
  }

  playMusic (url) {
    if (this.musicLoading) { return false }

    this.stopMusic()

    this.musicLoading = true
    this._loadFile(url).then(
      audio => {
        this.musicLoading = false
        this.music.setBuffer(audio)
        this.music.setLoop(true)
        this.music.play()
      },
      error => {
        this.musicLoading = false
        console.log('Could not load music file', url)
      }
    )

    return true
  }

  stopMusic () {
    if (!this.music.isPlaying) { return false }
    this.music.stop()
    return true
  }

  musicPlaying () { return this.musicLoading || this.music.isPlaying }

  // Used internally for loading audio clips and music:
  _loadFile (url) {
    return new Promise((resolve, reject) => {
      const loader = new THREE.AudioLoader()
      loader.load(url, resolve, null, reject)
    })
  }

  _updateSound (sound) {
    if (!sound.panner) { return false } // Not a 3D positional sound.

    const sinTurn = Math.sin(this.turn)
    const cosTurn = Math.cos(this.turn)

    const dx = sound.position.x - this.listener.position.x
    const dy = sound.position.y - this.listener.position.y
    const dz = sound.position.z - this.listener.position.z

    sound.panner.setPosition(
      (dx * sinTurn) - (dy * cosTurn),
      (dx * cosTurn) + (dy * sinTurn),
      dz
    )

    return true
  }
}

export default GameAudio
