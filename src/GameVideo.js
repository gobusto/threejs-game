class GameVideo {
  // Initialise everything:
  constructor (selector, width, height) {
    this.initWebGL(width, height)

    const target = document.querySelector(selector)
    target.appendChild(this.renderer.domElement)

    this.init2D(width, height) // This ThreeJS scene represents the 2D overlay.
    this.init3D(width, height) // This ThreeJS scene represents the 3D world.

    // These are used to represent actors whilst we wait for the model to load:
    this.placeholderGeometry = new THREE.BoxGeometry()
    this.placeholderMaterial = new THREE.MeshPhongMaterial()
  }

  initWebGL (width, height) {
    this.renderer = new THREE.WebGLRenderer()
    this.renderer.setSize(width, height)
    this.renderer.setPixelRatio(window.devicePixelRatio)
    this.renderer.autoClear = false
    this.renderer.shadowMap.enabled = true
  }

  init2D (width, height) {
    this.images = {}

    this.overlay = new THREE.Scene()

    this.layout = new THREE.OrthographicCamera(-16/9, 16/9, 1, -1, -1, 1)
    this.layout.position.z = 1
  }

  init3D (width, height) {
    this.gridSize = 100 // This determines how many tiles we draw at once.

    this.camera = new THREE.PerspectiveCamera(60, width/height, 0.1, this.gridSize * 0.5)

    // TODO: Allow this to be configured on a per-map basis.
    const sky = 0xd0e0f0
    this.scene = new THREE.Scene()
    this.scene.background = new THREE.Color(sky)
    this.scene.fog = new THREE.Fog(sky, 0, this.gridSize * 0.35)

    // TODO: Allow this to be configured on a per-map basis.
    const ambient = new THREE.AmbientLight(0x505050)
    this.scene.add(ambient)

    // TODO: Allow this to be configured on a per-map basis.
    const sun = new THREE.DirectionalLight(0xC0C0C0)
    sun.position.set(1, 1, 1)
    this.scene.add(sun)

    let indices = []
    let v = []
    let uv = []
    for (let y = 0; y < this.gridSize; ++y) {
      for (let x = 0; x < this.gridSize; ++x) {
        if (x > 0 & y > 0) { indices.push(0, 0, 0, 0, 0, 0) }
        v.push(x, y, 0)
        uv.push(x, y)
      }
    }

    this.grid = new THREE.BufferGeometry()
    this.grid.setIndex(indices)
    this.grid.setAttribute('position', new THREE.Float32BufferAttribute(v, 3))
    this.grid.setAttribute('uv', new THREE.Float32BufferAttribute(uv, 2))
  }

  loadImage (name, url) {
    if (this.images[name]) { return false }
    this.images[name] = this.loadTexture(url)
    return true
  }

  // TODO: https://usefulangle.com/post/74/javascript-dynamic-font-loading
  loadFont (_name, url) {
    const link = document.createElement('link')
    link.rel = 'stylesheet'
    link.href = url
    document.body.append(link)
    return true
  }

  drawText(sprite, font, align, color, text) {
    if (!sprite || !sprite.textImage) { return } // Not a text sprite.

    const ctx = sprite.canvas.getContext('2d')
    ctx.font = ['50px', font].join(' ')
    ctx.clearRect(0, 0, sprite.canvas.width, sprite.canvas.height)
    ctx.fillStyle = color

    let x = 0
    if (align === 'right') {
      x = sprite.canvas.width - ctx.measureText(text).width
    } else if (align === 'center') {
      x = (sprite.canvas.width / 2) - (ctx.measureText(text).width / 2)
    }
    ctx.fillText(text, x, 50)

    sprite.textImage.needsUpdate = true
  }

  // Texture loading doesn't work for file:// URLs on Chrome... :/
  // Note that textures and materials are different things!
  loadTexture (url) {
    const textureLoader = new THREE.TextureLoader()
    const texture = textureLoader.load(url)
    texture.wrapS = texture.wrapT = THREE.RepeatWrapping
    return texture
  }

  cacheMaterials (world) {
    if (this.heightmapInstance) { return }

    this.materials = world.tileTypes.map(s => new THREE.MeshPhongMaterial({ map: this.loadTexture(s.diffuse) }))
    this.heightmapInstance = new THREE.Mesh(this.grid, this.materials)
    this.heightmapInstance.receiveShadow = true
    this.scene.add(this.heightmapInstance)
  }

  update (world, stage) {
    this.renderer.clear()

    if (world) {
      this.draw3D(world)
      this.renderer.render(this.scene, this.camera)
    }

    if (world && stage) { this.renderer.clearDepth() }

    if (stage) {
      this.draw2D(stage)
      this.renderer.render(this.overlay, this.layout)
    }
  }

  draw2D (stage) {
    stage.getSprites().forEach(sprite => {
      if (!sprite.gfxObject) {
        if (sprite.kind === 'text') {
          let canvas = document.createElement('canvas')
          canvas.width = 1024 // Max. supported texture width.
          canvas.height = 64 // 50px rounded to a power of 2.

          let texture = new THREE.CanvasTexture(canvas)
          let material = new THREE.SpriteMaterial({ map: texture })
          let instance = new THREE.Sprite(material)
          sprite.gfxObject = { instance: instance, textImage: texture, canvas: canvas }
        } else {
          let texture = this.images[sprite.image]
          let material = new THREE.SpriteMaterial({ map: texture })
          let instance = new THREE.Sprite(material)
          sprite.gfxObject = { instance: instance }
        }

        this.overlay.add(sprite.gfxObject.instance)
      }

      if (sprite.kind === 'text') {
        const horizontalAnchor = { center: 0.5, right: 1 }[sprite.align] || 0
        sprite.gfxObject.instance.center.set(horizontalAnchor, 0.5)
        this.drawText(sprite.gfxObject, sprite.font, sprite.align, sprite.color, sprite.text)
      } else {
        sprite.gfxObject.instance.material.color.set(sprite.color)
        sprite.gfxObject.instance.material.map = this.images[sprite.image]
      }

      sprite.gfxObject.instance.position.set(
        sprite.position[0],
        sprite.position[1],
        0
      )

      const canvas = sprite.gfxObject.canvas
      sprite.gfxObject.instance.scale.set(
        (canvas ? canvas.width / canvas.height : 1) * sprite.scale[0],
        sprite.scale[1],
        1
      )

      sprite.gfxObject.instance.material.opacity = sprite.opacity
      sprite.gfxObject.instance.material.rotation = sprite.angle
    })
  }

  draw3D (world) {
    this.camera.setRotationFromMatrix(new THREE.Matrix4())
    this.camera.rotateX(Math.PI/2)
    this.camera.rotateY(-Math.PI/2)

    this.camera.position.x = world.x
    this.camera.position.y = world.y
    this.camera.position.z = world.z

    this.camera.rotateY(world.turn)
    this.camera.rotateX(-world.look)
    this.camera.rotateZ(world.roll)

    this.updateWater(world)
    this.updateTiles(world)
    this.cacheMaterials(world)

    world.getActors().forEach(actor => {
      const prop = this.getVisualRepresentation(actor)
      prop.position.set(actor.position[0], actor.position[1], actor.position[2])
      prop.setRotationFromMatrix(new THREE.Matrix4())
      prop.rotateZ(actor.turn)
      prop.rotateY(actor.look)
      prop.rotateX(actor.roll)
    })
  }

  updateWater (world) {
    if (!world.water) { return }

    if (!this.water) {
      this.waterTexture = this.loadTexture(world.water.diffuse)
      this.waterTexture.repeat.set(this.gridSize, this.gridSize)

      this.waterMaterial = new THREE.MeshLambertMaterial({ map: this.waterTexture })
      this.waterMaterial.side = THREE.DoubleSide
      this.waterMaterial.transparent = world.water.opacity < 1
      this.waterMaterial.opacity = world.water.opacity

      this.waterGeometry = new THREE.PlaneBufferGeometry(this.gridSize, this.gridSize)

      this.water = new THREE.Mesh(this.waterGeometry, this.waterMaterial)
      this.scene.add(this.water)
    }

    this.waterTexture.offset.x += 0.001
    this.waterTexture.offset.y += 0.002
    this.water.position.set(Math.floor(world.x), Math.floor(world.y), world.water.height)
  }

  updateTiles (world) {
    const offsetX = Math.floor(world.x) - Math.floor(this.gridSize / 2)
    const offsetY = Math.floor(world.y) - Math.floor(this.gridSize / 2)

    // Minimise geometry updates by checking for "big" camera position changes:
    if (this.oldX && this.oldX === offsetX && this.oldY === offsetY) { return }
    this.oldX = offsetX
    this.oldY = offsetY

    let groups = {}
    for (let y = 0; y < this.gridSize; ++y) {
      for (let x = 0; x < this.gridSize; ++x) {
        const tile = world.getTile(offsetX + x, offsetY + y)

        // Update tile heights (and offset, since it moves with the camera):
        const offset = (3 * y * this.gridSize) + (3 * x)
        this.grid.attributes.position.array[offset + 0] = offsetX + x
        this.grid.attributes.position.array[offset + 1] = offsetY + y
        this.grid.attributes.position.array[offset + 2] = tile.height

        // Add indices to the relevant material group:
        if (x > 0 & y > 0) {
          if (!groups[tile.materialIndex]) { groups[tile.materialIndex] = [] }

          groups[tile.materialIndex].push(
            ((y - 1) * this.gridSize) + (x - 1),
            ((y - 1) * this.gridSize) + (x - 0),
            ((y - 0) * this.gridSize) + (x - 1),
            ((y - 0) * this.gridSize) + (x - 0),
            ((y - 0) * this.gridSize) + (x - 1),
            ((y - 1) * this.gridSize) + (x - 0)
          )
        }
      }
    }
    this.grid.attributes.position.needsUpdate = true

    // Flatten the per-group indices into the actual "indices" array:
    let start = 0
    this.grid.clearGroups()
    Object.keys(groups).forEach(materialIndex => {
      const indices = groups[materialIndex]
      indices.forEach((value, n) => this.grid.index.array[start + n] = value)

      const count = indices.length
      this.grid.addGroup(start, count, materialIndex)

      start += count
    })
    this.grid.index.needsUpdate = true

    // Recompute geometry information:
    this.grid.computeBoundingSphere()
    this.grid.computeVertexNormals()
  }

  getVisualRepresentation (actor) {
    if (!actor.gfxObject) {
      actor.gfxObject = new THREE.Group()

      // Use a placeholder UNLESS `actor.klass.mesh` is explicitly left blank:
      if (!actor.klass || actor.klass.mesh) {
        const size = actor.aabb()
        actor.gfxPlaceholder = new THREE.Mesh(this.placeholderGeometry, this.placeholderMaterial)
        actor.gfxPlaceholder.scale.set(size[0], size[1], size[2])
        actor.gfxObject.add(actor.gfxPlaceholder)
      }

      // NOTE: Adding a light is quite slow; can we speed this up somehow?
      actor.klass && actor.klass.lights.forEach(bulb => {
        const light = new THREE.PointLight(bulb.color, 1, bulb.distance)
        light.position.set(bulb.position[0], bulb.position[1], bulb.position[2])
        light.castShadow = bulb.shadows
        actor.gfxObject.add(light)
      })

      this.scene.add(actor.gfxObject)
    }

    // Once the "real" model loads, use that instead of the placeholder:
    if (actor.gfxPlaceholder && actor.klass && actor.klass.mesh && actor.klass.mesh !== 'loading') {
      const group = actor.klass.mesh.clone()
      group.children.forEach(child => child.castShadow = true)
      group.children.forEach(child => child.receiveShadow = true)
      actor.gfxObject.add(group)

      actor.gfxObject.remove(actor.gfxPlaceholder)
      actor.gfxPlaceholder = null
    }

    return actor.gfxObject
  }

  enableFullscreen () { this.renderer.domElement.requestFullscreen() }
}

export default GameVideo
