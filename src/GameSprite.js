class GameSprite {
  constructor (json) {
    if (json.image) {
      this.kind = 'image'
      this.image = json.image
    } else {
      this.kind = 'text'
      this.font = json.font || 'sans-serif'
      this.text = json.text || ''
      this.align = json.align || 'left'
    }

    this.color = '#FFFFFF'
    this.opacity = 1.0
    this.position = [0, 0]
    this.scale = [json.scale || 1, json.scale || 1]
    this.angle = 0
  }

  setImage (value) {
    if (this.kind !== 'image') { return false }
    this.image = String(value)
    return true
  }

  setFont (value) {
    if (this.kind !== 'text') { return false }
    this.font = String(value)
    return true
  }

  setText (value) {
    if (this.kind !== 'text') { return false }
    this.text = String(value)
    return true
  }

  setAlign (value) {
    if (this.kind !== 'text') { return false }
    this.align = String(value)
    return true
  }

  setColor (value) {
    this.color = value
  }

  setOpacity (value) {
    this.opacity = value
  }

  setAngle (value) {
    this.angle = value
  }

  setPosition (x, y) {
    this.position[0] = x
    this.position[1] = y
  }

  setScale (x, y) {
    this.scale[0] = x
    this.scale[1] = y
  }
}

export default GameSprite
