class GameActor {
  constructor (klass, json, api) {
    this.kind = json.kind // Useful for script logic.
    this.klass = klass

    this.position = json.position ? json.position.slice() : [0, 0, 0]
    this.velocity = [0, 0, 0]

    this.turn = json.turn || 0
    this.look = 0
    this.roll = 0

    this.gravity = this.klass ? !this.klass.antigravity : true
    this.static = this.klass ? this.klass.static : false
    this.friction = this.klass ? this.klass.friction : 1.0
    if (!this.klass) { return console.warn('Unknown actor type', json.kind) }

    if (this.klass.script) { this.script = new this.klass.script(api, this) }

    if (Array.isArray(json.messages)) {
      json.messages.forEach(message => this.sendMessage(api, message))
    }
  }

  think (api, input) {
    if (this.script && this.script.think) { this.script.think(api, input) }
  }

  collide (api, other) {
    const collideScript = this.script && this.script.collide
    return collideScript ? this.script.collide(api, other) : true
  }

  sendMessage (api, json) {
    const messageScript = this.script && this.script.message
    return messageScript ? this.script.message(api, json) : null
  }

  setPosition (x, y, z) {
    this.position[0] = x
    this.position[1] = y
    this.position[2] = z
  }

  addForce (x, y, z) {
    this.velocity[0] += x
    this.velocity[1] += y
    this.velocity[2] += z
  }

  aabb () {
    return this.klass ? this.klass.size : [1, 1, 1]
  }
}

export default GameActor
