import GameAudio from './GameAudio.js'
import GameClass from './GameClass.js'
import GameStage from './GameStage.js'
import GameVideo from './GameVideo.js'
import GameWorld from './GameWorld.js'
import Keyboard from './util/Keyboard.js'
import Joypad from './util/Joypad.js'

class GameEngine {
  constructor (selector) {
    this.stage = new GameStage()

    this.loadingProgress = {}
    this.loadingSprite = this.stage.create({ scale: 0.2, align: 'center' })

    this.video = new GameVideo(selector, 640, 360)

    this.loadFile('base/images.json', data => {
      const json = JSON.parse(data)
      Object.keys(json).forEach(name => this.video.loadImage(name, json[name]))
    })

    this.loadFile('base/fonts.json', data => {
      const json = JSON.parse(data)
      Object.keys(json).forEach(name => this.video.loadFont(name, json[name]))
    })

    this.joypad = new Joypad()

    // Initialise keyboard input:
    this.keyboard = new Keyboard()
    this.keyboard.attach(document.body)

    // Handle transitions to/from full-screen mode:
    document.onfullscreenchange = () => {
      if (document.fullscreenElement) {
        this.keyboard.enableExclusiveMode()
        document.fullscreenElement.requestPointerLock()
      } else {
        this.keyboard.disableExclusiveMode()
      }
    }

    // Load a list of actor classes, and then load the default map file:
    this.classes = {}
    this.loadFile('base/actors.json', (text) => {
      const json = JSON.parse(text)
      Object.keys(json).forEach(k => this.classes[k] = new GameClass(json[k]))

      this.loadFile('maps/start.json', data => this.world = new GameWorld(
        this.classes, this.audio, this.stage, JSON.parse(data)
      ))
    })
  }

  enableAudio () {
    if (this.audio) { return false }
    this.audio = new GameAudio()

    this.loadFile('base/sounds.json', data => {
      const json = JSON.parse(data)
      Object.keys(json).forEach(name => this.audio.loadSound(name, json[name]))
    })

    return true
  }

  enableFullscreen () { this.video.enableFullscreen() }

  mainLoop (currentTime) {
    requestAnimationFrame(this.mainLoop.bind(this)) // Keep looping!
    if (!this.timer) { this.timer = currentTime }

    // If we are loading one or more files, show a progress indicator:
    const file = Object.keys(this.loadingProgress)[0]
    if (file) {
      const sizeInBytes = this.loadingProgress[file].total // May not be known!
      const sizeInKilos = sizeInBytes ? Math.ceil(sizeInBytes / 1024) : '???'
      const loadedKilos = Math.floor(this.loadingProgress[file].loaded / 1024)
      this.loadingSprite.setText(`${loadedKilos} / ${sizeInKilos} KB`)
    } else {
      this.loadingSprite.setText('') // Hide it if nothing is being loaded.
    }

    let updated = false
    while (this.timer <= currentTime) {
      const input = this.getInput()

      if (this.world) {
        this.world.update(this.classes, this.audio, this.stage, input)
      }
      this.stage.clear()

      this.timer += 1000 / 60
      updated = true
    }

    if (updated) { this.video.update(this.world, this.stage) }
  }

  getInput () {
    let input = {
      lx: 0,
      ly: 0,
      rx: 0,
      ry: 0,
      a: false,
      b: false,
      x: false,
      y: false
    }

    // First, see if there's a joypad attached:
    this.joypad.update()
    input.lx = this.joypad.leftStickX()
    input.ly = this.joypad.leftStickY()
    input.rx = this.joypad.rightStickX()
    input.ry = this.joypad.rightStickY()
    input.a = this.joypad.face1()
    input.b = this.joypad.face2()
    input.x = this.joypad.face3()
    input.y = this.joypad.face4()

    // Map keyboard input so that it looks the same as joypad input:
    const keyboard = this.keyboard.getState()
    if (keyboard['A']) { input.lx = -1 }
    if (keyboard['D']) { input.lx = 1 }
    if (keyboard['W']) { input.ly = -1 }
    if (keyboard['S']) { input.ly = 1 }
    if (keyboard['ARROWLEFT'])  { input.rx = -1 }
    if (keyboard['ARROWRIGHT']) { input.rx = 1 }
    if (keyboard['ARROWUP'])    { input.ry = -1 }
    if (keyboard['ARROWDOWN'])  { input.ry = 1 }
    if (keyboard[' '])  { input.a = true }
    if (keyboard['SHIFT'])  { input.b = true }
    if (keyboard['CONTROL'])  { input.x = true }
    if (keyboard['TAB'])  { input.y = true }

    // Joypad deadzone handling:
    if (Math.abs(input.lx) < 0.2) { input.lx = 0 }
    if (Math.abs(input.ly) < 0.2) { input.ly = 0 }
    if (Math.abs(input.rx) < 0.2) { input.rx = 0 }
    if (Math.abs(input.ry) < 0.2) { input.ry = 0 }

    return input
  }

  loadFile (url, resolve) {
    const loader = new THREE.FileLoader()

    loader.load(
      url,
      result => {
        delete this.loadingProgress[url]
        resolve(result)
      },
      progress => {
        const value = { loaded: progress.loaded, total: progress.total }
        this.loadingProgress[url] = value
      },
      error => {
        delete this.loadingProgress[url]
        console.log('loadFile():', error.target.statusText)
      }
    )
  }
}

export default GameEngine
