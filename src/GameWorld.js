import GameActor from './GameActor.js'
import HeightGrid from './util/HeightGrid.js'

class GameWorld {
  constructor (classSet, audio, stage, json) {
    this.tileTypes = Array.isArray(json.tileTypes) ? json.tileTypes : []
    this.tiles = Array.isArray(json.tiles) ? json.tiles.reverse() : []

    this.height = null
    if (Array.isArray(json.heightmap)) {
      this.height = new HeightGrid(
        json.heightmap[0].length,
        json.heightmap.length,
      )
      json.heightmap.reverse().forEach((row, y) => {
        row.forEach((val, x) => this.height.setPoint(x, y, val))
      })
    }

    this.water = null
    if (json.water) {
      this.water = { diffuse: json.water.diffuse }
      this.water.opacity = json.water.opacity || 1
      this.water.height = json.water.height || 0
    }

    // Camera position:
    this.x = 0
    this.y = 0
    this.z = 0
    this.turn = 0
    this.look = 0
    this.roll = 0

    // This MUST come last, as actor script constructors may call API functions:
    const api = this._api(classSet, audio, stage)

    this.actors = []
    if (Array.isArray(json.actors)) {
      this.actors = json.actors.map(a => new GameActor(classSet[a.kind], a, api))
    }
  }

  update (classSet, audio, stage, input) {
    const api = this._api(classSet, audio, stage)

    // Update AI state:
    this.actors.forEach(actor => actor.think(api, input))

    // Update physics:
    this.actors.filter(actor => !actor.static).forEach(actor => {
      const halfActor = actor.aabb().map(x => x / 2)

      actor.onSurface = false
      actor.underwater = this.water && actor.position[2] < this.water.height
      if (actor.velocity[2] > -9.8 && actor.gravity) { actor.velocity[2] -= 0.01 }

      const friction = 1 / (1 + actor.friction)
      actor.velocity[0] *= friction
      actor.velocity[1] *= friction
      if (actor.underwater || !actor.gravity) { actor.velocity[2] *= friction }

      [0, 1, 2].forEach(axis => {
        const oldPosition = actor.position[axis]

        actor.position[axis] += actor.velocity[axis]

        this.actors.forEach(other => {
          const halfOther = other.aabb().map(x => x / 2)

          if (
            actor.position[0] - halfActor[0] < other.position[0] + halfOther[0] &&
            actor.position[0] + halfActor[0] > other.position[0] - halfOther[0] &&
            actor.position[1] - halfActor[1] < other.position[1] + halfOther[1] &&
            actor.position[1] + halfActor[1] > other.position[1] - halfOther[1] &&
            actor.position[2] - halfActor[2] < other.position[2] + halfOther[2] &&
            actor.position[2] + halfActor[2] > other.position[2] - halfOther[2] &&
            actor !== other && !actor.destroy && !other.destroy
          ) {
            // Make sure we call the on-collide method for BOTH actors, in case
            // they are destroy-on-touch or similar:
            const actorSolid = actor.collide(api, other)
            const otherSolid = other.collide(api, actor)

            if (actorSolid && otherSolid) {
              actor.position[axis] = oldPosition
              if (axis === 2 && actor.velocity[2] < 0) { actor.onSurface = true }
              actor.velocity[axis] = 0
            }
          }
        })
      })

      if (this.height && !actor.destroy) {
        const p = this.height.getPoint(actor.position[0], actor.position[1])
        if (actor.position[2] <= p.z + halfActor[2] && actor.collide(api)) {
          actor.position[2] = p.z + halfActor[2]

          // If the incline is too steep, slide down it:
          if (Math.abs(p.mx) > 1) { actor.velocity[0] = p.mx * -0.02 }
          if (Math.abs(p.my) > 1) { actor.velocity[1] = p.my * -0.02 }

          // If the inclines are both fairly flat, stand on it:
          if (Math.abs(p.mx) <= 1 && Math.abs(p.my) <= 1) {
            actor.velocity[2] = 0
            actor.onSurface = true
          }
        }
      }

    })

    // HACK: This should be done by the graphics code, somehow:
    this.actors.forEach(actor => {
      if (!actor.destroy || !actor.gfxObject) { return }
      actor.gfxObject.parent.remove(actor.gfxObject)
    })

    this.actors = this.actors.filter(actor => !actor.destroy)
  }

  getTile (x, y) {
    const materialIndex = (this.tiles[y] || [])[x] || 0
    const height = this.height ? this.height.getPoint(x, y).z : 0 // Lower left
    return { height: height, materialIndex: materialIndex }
  }

  getActors () {
    return this.actors
  }

  // TODO: Could we split this out into a separate class/file for tidiness...?
  _api (classSet, audio, stage) {
    const api = {
      audioEnabled: () => !!audio,
      musicPlaying: () => audio && audio.musicPlaying(),
      playMusic: url => (
        audio && audio.playMusic(url)
      ),
      stopMusic: () => (
        audio && audio.stopMusic()
      ),
      playSound: (name, loop) => (
        audio && audio.playSound(name, { loop: loop })
      ),
      playSound3D: (name, x, y, z, loop) => (
        audio && audio.playSound(name, { position: [x, y, z], loop: loop })
      ),
      stopSound: (soundID) => (
        audio && audio.stopSound(soundID)
      ),
      setSoundPosition: (soundID, x, y, z) => (
        audio && audio.setSoundPosition(soundID, x, y, z)
      ),
      setSoundVolume: (soundID, value) => (
        audio && audio.setSoundVolume(soundID, value)
      ),
      createText: (text) => (
        stage && stage.create({ text: text })
      ),
      createSprite: (image) => (
        stage && stage.create({ image: image })
      ),
      getCamera: () => {
        return {
          position: [this.x, this.y, this.z],
          turn: this.turn,
          look: this.look,
          roll: this.roll
        }
      },
      setCamera: (x, y, z, turn, look, roll) => {
        this.x = x
        this.y = y
        this.z = z
        this.turn = turn
        this.look = look
        this.roll = roll
        audio && audio.setListenerPosition(x, y, z, turn, look, roll)
      },
      getActors: () => this.actors
    }

    // Special case: Actors created via the API can call that same API:
    api.createActor = (kind, x, y, z, turn) => {
      const json = { kind: kind, position: [x, y, z], turn: turn }
      const actor = new GameActor(classSet[kind], json, api)
      this.actors.push(actor)
      return actor
    }

    return api
  }
}

export default GameWorld
