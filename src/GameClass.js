import MeshLoader from './util/MeshLoader.js'

class GameClass {
  constructor (json) {
    const scale = json.scale || 1

    this.friction = json.friction === undefined ? 1 : Number(json.friction)
    if (this.friction < 0) { this.friction = 0 }

    this.size = [
      json.radius || scale,
      json.radius || scale,
      json.height || scale
    ]

    if (json.script) {
      import(`../${json.script}`).then(
        jsmod => this.script = jsmod.default,
        error => console.log(error)
      )
    }

    if (json.mesh) {
      this.loadMesh(json.mesh, json.skin, json.radius, json.height, scale)
    }

    this.lights = []
    if (Array.isArray(json.lights)) {
      json.lights.forEach(light => {
        let xyz = [0, 0, 0]
        if (Array.isArray(light.position)) { xyz = light.position }

        this.lights.push({
          color: parseInt(light.color) || 0xFFFFFF,
          distance: light.distance || 10,
          position: [xyz[0] || 0, xyz[1] || 0, xyz[2] || 0],
          shadows: light.shadows || false
        })
      })
    }

    // A static actor isn't subject to physics, but actors can collide with it:
    this.static = json.static
    // Antigravity actors treat the Z axis in the same way as the X and Y axes:
    this.antigravity = json.antigravity
  }

  loadMesh (mesh, skin, radius, height, scale) {
    this.mesh = 'loading' // We DO expect to have a mesh - just not yet!

    const meshLoader = new MeshLoader()
    meshLoader.load(mesh, skin).then(
      group => {
        group.children.forEach(m => m.geometry.scale(scale, scale, scale))

        const aabb = this.calculateAABB(group)

        this.size = [
          radius || aabb.max[0] - aabb.min[0],
          radius || aabb.max[1] - aabb.min[1],
          height || aabb.max[2] - aabb.min[2]
        ]

        // Actors can turn, so use the biggest value for the X/Y axes:
        if (this.size[0] < this.size[1]) { this.size[0] = this.size[1] }
        if (this.size[1] < this.size[0]) { this.size[1] = this.size[0] }
        console.debug(`${mesh}: ${this.size[0]}x${this.size[1]}x${this.size[2]}`)

        group.children.forEach(model => model.geometry.translate(
          0 - (aabb.min[0] + ((aabb.max[0] - aabb.min[0]) / 2.0)),
          0 - (aabb.min[1] + ((aabb.max[1] - aabb.min[1]) / 2.0)),
          0 - (aabb.min[2] + ((aabb.max[2] - aabb.min[2]) / 2.0))
        ))

        this.mesh = group
      },
      _ => console.log('Could not load mesh:', mesh)
    )
  }

  calculateAABB(group) {
    let aabb = { min: [null, null, null], max: [null, null, null] }

    group.children.forEach(model => {
      model.geometry.computeBoundingBox()

      const axes = ['x', 'y', 'z']
      axes.forEach((axis, i) => {
        const min = model.geometry.boundingBox.min[axis]
        const max = model.geometry.boundingBox.max[axis]
        if (aabb.min[i] === null || aabb.min[i] > min) { aabb.min[i] = min }
        if (aabb.max[i] === null || aabb.max[i] < max) { aabb.max[i] = max }
      })
    })

    return aabb
  }
}

export default GameClass
