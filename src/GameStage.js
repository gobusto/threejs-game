import GameSprite from './GameSprite.js'

class GameStage {
  constructor () {
    this.sprites = []
  }

  create (json) {
    const sprite = new GameSprite(json)
    this.sprites.push(sprite)
    return sprite
  }

  clear () {
    // HACK: This should be done by the graphics code, somehow:
    this.sprites.forEach(sprite => {
      if (!sprite.destroy || !sprite.gfxObject) { return }
      sprite.gfxObject.instance.parent.remove(sprite.gfxObject.instance)
    })

    this.sprites = this.sprites.filter(sprite => !sprite.destroy)
  }

  getSprites () { return this.sprites }
}

export default GameStage
