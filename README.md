THREE.JS TEST PROJECT
=====================

An attempt to use WebGL, WebAudio, and JS keyboard/mouse/gamepad input to build
a "game engine" comparable to something I'd normally make with SDL/OpenGL in C.

Compatibility
-------------

### Browsers

Firefox and Chrome are explicitly tested, but any modern browser should work.

Note that this project takes advantage of a lot of "newer" Javascript features,
such as `import` and template literals, so older versions of browsers (notably,
Microsoft Edge versions before v79) probably won't work.

### Devices/Platforms

PC/Mac machines running GNU/Linux, Windows, or macOS should work just fine.

The XBox One browser supports WebGL and gamepads; it reportedly runs everything
without problems, so I'll be trying to keep it that way.

> **NOTE:** The XBox One currently uses Edge v44, which doesn't support dynamic
> `import` (necessary for loading actor scripts), so it's currently broken; the
> next version (v79) does, so hopefully this problem will resolve itself soon.

I don't think that the Switch supports WebGL, so it probably doesn't work - see
[here (2017)](https://blog.webb.page/2017/nintendo-switch-browser-capabilities)
for details.

The PlayStation 4 is **not** supported, since its browser doesn't support WebGL
or the gamepad API and keyboard/mouse support is very incomplete.

Phones/tablets may be supported, depending on how awkward it is to handle touch
events, but aren't a high priority.

### Gamepads

Chrome correctly maps common pads (PS3/PS4, XBox 360/One, Nintendo Switch) to a
["standard"](https://developer.mozilla.org/en-US/docs/Web/API/Gamepad/mapping/)
layout.

Firefox supports gamepads, but doesn't map them to the standard layout yet; see
https://hg.mozilla.org/mozilla-central/rev/81f95326825c (dated 18 Apr 2019) for
the fix, which should hopefully make its way into an official release soon.

The gamepad API does not officially support haptic features yet. Rumble support
is there in [Chrome 68](https://www.chromestatus.com/features/5705158763741184)
(with a draft specification written for other browsers to implement it), but it
is not yet standardised.

Problems of note
----------------

Some issues I ran into during testing, noted here for future reference.

### Audio playback is blocked by browsers unless the user does something first.

This is an intentional "anti-annoyance" feature added by the Chrome developers:

<https://discourse.threejs.org/t/audio-stopped-playing-in-latest-chrome-update>

A similar restriction exists for unprompted fullscreen/mouse-lock attempts.

### Browsers don't allow files to be fetched via AJAX for `file://` URLs.

Use a local server: <https://stackoverflow.com/q/3108395/4200092>

### Chrome doesn't seem to support WebGL, but it works fine on Firefox.

Go to `chrome://flags` and enable **Override software rendering list**.

Useful Links
------------

For developers interested in how this project works:

+ <https://threejs.org/docs>
+ <https://developer.mozilla.org/en-US/docs/Web/API/Gamepad>
    + Rumble support: <https://github.com/w3c/gamepad/issues/19>
+ <https://developer.mozilla.org/en-US/docs/Web/API/Pointer_Lock_API>
+ <https://developer.mozilla.org/en-US/docs/Web/API/Fullscreen_API>

TODO
----

Things I want to do at some point:

+ Improve collision (allow cylinders and OBB instead of just AABB).
+ Handle mouse input when fullscreen.
+ Add support for controller vibration, if supported by the browser.
+ Implement saving/loading (using local storage?) of state.
+ Allow map-changing.
+ Implement model animation (FBX?).
+ Apply an visual "underwater" effect if the camera is underwater.
+ Add loading screens.
+ Allow map materials to specify properties for footstep sounds, etc.

BUGS
----

+ Certain OBJ models are sometimes invisible. (Possibly fixed?)
+ Can't pick up the sword sometimes. (Script-loading race condition?)

License
-------

Copyright 2019-2020 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
