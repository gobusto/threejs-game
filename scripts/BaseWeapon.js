class BaseWeapon {
  // Calculates a position for a "HUD" weapon, accounting for rotation:
  rotatedOffset (dx, dy, dz, turn, look) {
    const sinTurn = Math.sin(turn)
    const cosTurn = Math.cos(turn)
    const sinLook = Math.sin(look)
    const cosLook = Math.cos(look)

    return [
      cosTurn*cosLook*dx + sinTurn*dy + cosTurn*sinLook*dz,
      sinTurn*cosLook*dx - cosTurn*dy + sinTurn*sinLook*dz,
      cosLook*dz - sinLook*dx
    ]
  }
}

export default BaseWeapon
