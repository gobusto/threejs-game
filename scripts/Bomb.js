class Bomb {
  constructor(api, actor) {
    this.actor = actor
    this.counter = 0
  }

  think (api, input) {
    if (api.audioEnabled() && !this.sound) {
      this.sound = api.playSound3D('tick', 0, 0, 0, true)
    }

    if (this.sound) {
      api.setSoundPosition(
        this.sound,
        this.actor.position[0],
        this.actor.position[1],
        this.actor.position[2]
      )
    }

    ++this.counter
    if (this.counter === 200) { this.destroy(api) }
  }

  collide (api, other) {
    if (other) { this.destroy(api) }
    return true
  }

  destroy (api) {
    this.actor.destroy = true
    api.stopSound(this.sound)
    api.createActor(
      'explosion',
      this.actor.position[0],
      this.actor.position[1],
      this.actor.position[2],
      this.actor.turn
    )
  }
}

export default Bomb
