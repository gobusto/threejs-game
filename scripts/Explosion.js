class Explosion {
  constructor(api, actor) {
    this.actor = actor
    this.counter = 0

    const boom = api.playSound3D(
      'explode',
      this.actor.position[0],
      this.actor.position[1],
      this.actor.position[2]
    )
    api.setSoundVolume(boom, 3.0)
  }

  think (api, input) {
    const camera = api.getCamera()
    const dx = camera.position[0] - this.actor.position[0]
    const dy = camera.position[1] - this.actor.position[1]
    this.actor.turn = Math.atan2(dy, dx)

    ++this.counter
    this.actor.destroy = this.counter >= 3
  }

  collide (api, other) { return false }
}

export default Explosion
