class AudioEmitter {
  constructor(api, actor) {
    this.actor = actor
    this.effect = null
  }

  think (api, input) {
    if (!this.sound && this.effect && api.audioEnabled()) {
      this.sound = api.playSound3D(this.effect, 0, 0, 0, true)
    }

    api.setSoundPosition(
      this.sound,
      this.actor.position[0],
      this.actor.position[1],
      this.actor.position[2]
    )
  }

  collide (api, other) {
    return false
  }

  message (api, data) {
    this.effect = data.audio
  }
}

export default AudioEmitter
