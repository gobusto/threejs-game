class Player {
  constructor(api, actor) {
    this.actor = actor
    this.spawnedAt = Date.now()

    this.crosshair = api.createSprite('crosshair')
    this.crosshair.setScale(0.18, 0.18)
    this.crosshair.setColor('#FF8888')

    this.url = api.createText('https://gitlab.com/gobusto/threejs-game')
    this.url.setScale(0.18, 0.18)
    this.url.setPosition(0, 0.86)
    this.url.setAlign('center')
    this.url.setFont('Righteous')

    this.timer = api.createText('hello')
    this.timer.setScale(0.15, 0.15)
    this.timer.setPosition(-1.7, -0.86)
    this.timer.setColor('#FFFF00')
    this.timer.setFont('VT323')

    this.coords = api.createText('hello')
    this.coords.setScale(0.15, 0.15)
    this.coords.setPosition(1.7, -0.86)
    this.coords.setAlign('right')
    this.coords.setColor('#00FFFF')
  }

  think (api, input) {
    const MOVE_SPEED = 0.04
    const TURN_SPEED = 0.04

    const sinTurn = Math.sin(this.actor.turn)
    const cosTurn = Math.cos(this.actor.turn)

    this.crosshair.setImage(this.actor.underwater ? 'crosshair-alt' : 'crosshair')
    this.crosshair.setAngle(this.actor.turn)

    this.url.setAngle(Math.sin(Date.now() / 200.0) * 0.04)
    this.timer.setText(`⏱️ ${(Date.now() - this.spawnedAt) / 1000}`)
    this.coords.setText(`📍 ${this.actor.position.map(v => Math.round(v))}`)

    if (api.audioEnabled() && !api.musicPlaying()) {
      api.playMusic('music/wind/wind1.ogg')
    }

    this.actor.velocity[0] += (input.lx * sinTurn * MOVE_SPEED) - (input.ly * cosTurn * MOVE_SPEED)
    this.actor.velocity[1] -= (input.lx * cosTurn * MOVE_SPEED) + (input.ly * sinTurn * MOVE_SPEED)

    if (this.actor.underwater) {
      if (input.a) {
        this.actor.velocity[2] = 0.1
      }
    } else {
      if (input.a && this.actor.onSurface) {
        this.actor.velocity[2] = 0.25
        api.playSound('jump')
      }
    }

    if (input.b && !this.actor.fireHeld) {
      const newActor = api.createActor(
        'bomb',
        this.actor.position[0] + (cosTurn * 2),
        this.actor.position[1] + (sinTurn * 2),
        this.actor.position[2],
        this.actor.turn
      )
      newActor.addForce(cosTurn * 3, sinTurn * 3, 0)
      api.playSound('throw')
    }
    this.actor.fireHeld = input.b

    this.actor.turn -= input.rx * TURN_SPEED
    this.actor.look += input.ry * TURN_SPEED

    if      (this.actor.look < -1.57) { this.actor.look = -1.57 }
    else if (this.actor.look >  1.57) { this.actor.look =  1.57 }

    api.setCamera(
      this.actor.position[0],
      this.actor.position[1],
      this.actor.position[2] + 0.6,
      this.actor.turn,
      this.actor.look,
      this.actor.roll
    )
  }

  collide (api, other) {
    if (other && other.kind === 'short-sword') {
      other.sendMessage(api, { owner: this.actor })
      return false
    }

    return true
  }
}

export default Player
