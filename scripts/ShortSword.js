import BaseWeapon from './BaseWeapon.js'

class ShortSword extends BaseWeapon {
  constructor(api, actor) {
    super()
    this.actor = actor
    this.owner = null
  }

  think (api, input) {
    if (!this.owner) { return }

    const t = Date.now()
    const f = (
      Math.abs(this.owner.velocity[0]) +
      Math.abs(this.owner.velocity[1])
    )

    this.actor.turn = this.owner.turn
    this.actor.look = this.owner.look + 0.4
    this.actor.roll = Math.sin(t * 0.002) * 0.03

    const offset = this.rotatedOffset(
      0.23,
      0.12 + (Math.sin(t * 0.008) * f * 0.1),
      0.00 + (Math.sin(t * 0.016) * f * 0.01),
      this.owner.turn,
      this.owner.look
    )

    this.actor.setPosition(
      this.owner.position[0] + offset[0],
      this.owner.position[1] + offset[1],
      this.owner.position[2] + offset[2] + 0.6,
    )
  }

  collide (api, other) { return !this.owner }

  message (api, data) {
    this.owner = data.owner
    this.actor.static = !!this.owner
  }
}

export default ShortSword
