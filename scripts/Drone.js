class Drone {
  constructor(api, actor) {
    this.actor = actor
    this.timer = 0

    this.warning = api.createText('Drone Tracking Mode Activated')
    this.warning.setFont('Righteous')
    this.warning.setAlign('center')
    this.warning.setScale(0.14, 0.14)
    this.warning.setPosition(0, -0.5)
  }

  think (api, input) {
    const player = api.getActors().find(a => a.kind === 'player')
    if (!player) { return }

    const dx = player.position[0] - this.actor.position[0]
    const dy = player.position[1] - this.actor.position[1]
    const dz = (player.position[2] + 0.7) - this.actor.position[2]

    const TARGET_DISTANCE = 5
    const distance = Math.sqrt(dx*dx + dy*dy)

    const FORCE = 0.00004
    const near = distance < TARGET_DISTANCE
    this.actor.addForce((near ? 0 : dx) * FORCE, (near ? 0 : dy) * FORCE, dz * FORCE)

    const WOBBLE = FORCE * 1.2
    const wx = (Math.random() - 0.5) * WOBBLE
    const wy = (Math.random() - 0.5) * WOBBLE
    const wz = (Math.random() - 0.5) * WOBBLE
    this.actor.addForce(wz, wy, wz)

    this.actor.turn = Math.atan2(dy, dx)

    if (this.timer < 400) {
      if (this.timer >= 200) {
        this.warning.setOpacity(Math.sin((this.timer / 400) * Math.PI))
      }
      this.timer++
    } else if (this.warning) {
      this.warning.destroy = true
      this.warning = null // Stop referencing it, so it can be GC'd.
    }

    if (api.audioEnabled() && !this.hum) {
      this.hum = api.playSound3D('fan', 0, 0, 0, true)
    }

    if (this.hum) {
      api.setSoundVolume(this.hum, 0.2)
      api.setSoundPosition(
        this.hum,
        this.actor.position[0],
        this.actor.position[1],
        this.actor.position[2]
      )
    }

    if (api.audioEnabled() && !this.beep) {
      this.beep = api.playSound3D('robotics', 0, 0, 0, true)
    }

    if (this.beep) {
      api.setSoundVolume(this.beep, 0.2)
      api.setSoundPosition(
        this.beep,
        this.actor.position[0],
        this.actor.position[1],
        this.actor.position[2]
      )
    }

  }
}

export default Drone
